**Please install Node.js before using**

On first use   
run

> npm install http-server -g

# How to run

> npm start  

or

> http-server

then open

> http://localhost:8080

in your browser
