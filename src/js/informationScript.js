    function testing() {
        //AquaInfo.getMinorFromApp("HeyBaByyy!!!");
        alert('Mission Complete!');
    }

    function getAquariumData( minorNumber ) {
    	
    	var aquariumOwner = ""

    	switch (minorNumber) {
            case 15001:
                aquariumOwner = "AQUARIUM";
                break;
            case 15110:
                 aquariumOwner = "Mo";
                 break;
            case 15109:
                 aquariumOwner = "June";
                 break;
            case 15104:
                 aquariumOwner = "Masa-san";
                 break;
            case 15090:
                 aquariumOwner = "Mori-san";
                 break;
            case 15070:
                 aquariumOwner = "Sensei";
                 break;
            case 15086:
                 aquariumOwner = "Matsui-san";
                 break;
            case 303:
                 aquariumOwner = "Mo's iPhone";
                 break;
            default:
                 aquariumOwner = "Unknown";
        }

    	document.getElementById("title").innerHTML = aquariumOwner + "\'s Aquarium";
    	document.getElementById("aqID").innerHTML = minorNumber;
    	document.getElementById("owner").innerHTML = aquariumOwner;
    	
    }